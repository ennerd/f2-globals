# f2/globals

Micro library enabling sharing of state without introducing global variables or
using the $_GLOBALS super-global.

## Using:

```php
<?php
use function F2\globals;

function example() {
    $globals = &globals("package/name");

    print_r($globals);
    $globals[] = date("Y-m-d H:i:s");
}
```

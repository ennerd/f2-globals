<?php
namespace F2;

/**
 * f2/globals
 *
 * Provides a function storing global namespaced state outside of the $_GLOBALS variable
 *
 * Either modify variable directly: `F2\globals('your/library')["key"] = "value"` or
 * get a reference: `$state = &F2\globals('your/library')`.
 *
 * @param $namespace A string identifying your library, typically equal to your composer package name.
 * @return &array
 */
function &globals(string $namespace, string $sub=null) {
    static $state = [];
    if (!isset($state[$namespace])) {
        $state[$namespace] = [];
    }

    if ($sub !== null) {
        if(!isset($state[$namespace][$sub])) {
            $state[$namespace][$sub] = [];
        }
        return $state[$namespace][$sub];
    }

    return $state[$namespace];
}

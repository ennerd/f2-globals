<?php
namespace F2;
require("globals.php");

// Microscopic alternative to the PHP assert() function
function unitTest($truthy, $else=null, $exitCode=1) {
    if(!$truthy) {
        if($else !== null) {
            fwrite(STDERR, $else."\n");
        } else {
            $prev = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1)[0];
            fwrite(STDERR, 'Failed assertion at '.$prev['file'].":".$prev['line']."\n");
        }
        exit($exitCode);
    }
}


$globals = &globals('some/namespace');

unitTest(is_array($globals));
unitTest(empty($globals));

$globals['foo'] = 'bar';

$foobars = &globals('some/namespace');
unitTest(is_array($foobars));
unitTest(!empty($foobars));
unitTest(!empty($globals));

$barfoos = globals('some/namespace');
unitTest(is_array($barfoos));
unitTest(!empty($barfoos));
unset($barfoos['foo']);
unitTest(empty($barfoos));
unitTest(!empty($foobars));

echo "ALL OK\n";
